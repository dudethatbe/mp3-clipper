#!/bin/bash

get_output_filename() {
  filepath="$1"
  
  filename="${filepath%.*}"  # Remove extension
  extension="${filepath##*.}"  # Extract extension

  output_filename="${filename}_out.$extension"

  echo "$output_filename"
}

if [[ $# -ne 1 ]]; then
    echo "Usage: $0 <input_mp3_file>"
    exit 1
fi

input_file=$1

duration_int=$(ffprobe -i "$input_file" -show_format -v quiet -print_format json | jq '.format.duration'| xargs | cut -d. -f1)

echo "duration $duration_int"

# 30 minutes = 1800 seconds
num_segments=$(( duration_int / 1800 ))

echo "30 minute chunks $num_segments"

offset=50

# make clips
for (( i=0; i<$num_segments; i++ )); do
  chunk=$(( 1800 * i ))
  start_time=$(( offset + chunk ))
  echo "$offset_start $start_time"
  ffmpeg -hide_banner -y -i "$input_file" -ss $start_time -t 10 -c copy "output_$((i + 1)).mp3"
done

temp_file="./temp.mp3"
output_file=$(get_output_filename "$input_file")
mp3_files=( output_*.mp3 )
counter=0

until [ $counter -eq ${#mp3_files[@]} ]; do
  current_file="${mp3_files[$counter]}"
  echo "Processing file: $current_file"
  # check if this is the first file
  if [ $counter -eq 0 ]; then
    # copy the first file to the output (n = 0)
    ffmpeg -hide_banner -y -i "$current_file" -filter:a "afade=in:d=0.5" "$output_file"
    cp "$output_file" "$temp_file"
  else
    # concat temp file with current file (n = 1 + i)
    ffmpeg -hide_banner -y -i "$output_file" -i "$current_file" -filter_complex "[0][1]acrossfade=d=3:c1=0.2" "$temp_file"
    mv "$temp_file" "$output_file"
  fi

  ((counter++))
done

rm "$temp_file"