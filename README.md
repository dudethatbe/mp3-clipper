# mp3-clipper
this script will take a longform mp3 (2hrs minimum) and generate a preview of 4 sections every 30 mins.

## requirements
* unix environment
* ffmpeg

## config
```
$ chmod u+x script.sh
```
## usage
```
$ ./script.sh my-mp3-file.mp3
```